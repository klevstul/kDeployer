# info

- make sure all files are up to date, with correct project info
- run `deploy.sh` to deploy kDeployer into project
- build the environment(s)

note:  
kDeployer needs to run in the same location where the built files end up (build target). that is
the reason why `deploy.sh` has to be executed, so that kDeployer can run in the "build target".
`environmentBuilder.sh`, despite running it from inside kDeployer, will execute `kDeployer.pl`
inside "build target".

a (partly?) fix for this is suggested in https://gitlab.com/klevstul/kDeployer/issues/10.
