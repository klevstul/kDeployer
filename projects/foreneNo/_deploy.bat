ECHO OFF
:: CLS

:: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
:: what:        deploy script, to deploy kDeployer into a project
:: author:      frode klevstul (frode at klevstul dot com)
:: started:     may 2016
:: - - - - -
:: xcopy documentation:
:: http://ss64.com/nt/xcopy.html
:: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


:: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
:: PROJECT SPECIFIC CONFIGURATION - TO BE CONFIGURED FOR EACH PROJECT
:: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

:: - - - - -
:: set base target dir - full path to project specific kDeployer folder
:: note: this directory will be created, if needed
:: - - - - -
SET BASE_TARGET_DIR=R:\googleDrive\projects\_gitHub\foreNeno\kDeployer

:: - - - - -
:: project folder name - must be identical the the name of the folder, under the kDeployer "projects" directory
:: - - - - -
SET PROJECT_FOLDER=foreNeno


:: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
:: GENERIC CODE, THAT MOST LIKELY IS IDENTICAL IN BETWEEN PROJECTS, HENCE EDITING THE CODE BELOW SHOULD NOT BE NEEDED
:: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

SET TARGET_DIR=%BASE_TARGET_DIR%

:: - - - - - - -
:: deploy kDeployer.pl & libs to project
:: - - - - - - -
SET SOURCE_DIR=..\..\core
xcopy /D/Y %SOURCE_DIR%\kDeployer.pl %TARGET_DIR%\

:: - - - - - - -
:: deploy kClosureCompiler.pm to project
:: - - - - - - -
SET SOURCE_DIR=..\..\core
xcopy /D/Y %SOURCE_DIR%\kClosureCompiler.pm %TARGET_DIR%\

:: - - - - - - -
:: deploy kDeployer.ini to project
:: - - - - - - -
SET SOURCE_DIR="."
xcopy /D/Y %SOURCE_DIR%\kDeployer.ini %TARGET_DIR%\

:: - - - - - - -
:: deploy environments to project
:: - - - - - - -
SET SOURCE_DIR="environments"
xcopy /D/Y/E %SOURCE_DIR% %TARGET_DIR%\environments\

:: - - - - - - -
:: deploy project_excludedfileslist.txt to project
:: - - - - - - -
SET SOURCE_DIR="."
xcopy /D/Y %SOURCE_DIR%\project_excludedfileslist.txt %TARGET_DIR%\

:: - - - - - - -
:: deploy project_buildfilesexclude.txt to project
:: - - - - - - -
SET SOURCE_DIR="."
xcopy /D/Y %SOURCE_DIR%\project_buildfilesexclude.txt %TARGET_DIR%\

:: - - - - - - -
:: deploy copyToSourceFolder.bat to project
:: - - - - - - -
SET SOURCE_DIR=.
xcopy /D/Y %SOURCE_DIR%\copyToSourceFolder.bat %TARGET_DIR%\


:: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
:: CODE THAT IS NOT IN USE
:: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
::
:: :: make a copy of the initialisation file
:: copy "%TARGET_DIR%\kDeployer.ini" "%TARGET_DIR%\kDeployer.ini.backup.%DATE:~-4%-%DATE:~4,2%-%DATE:~7,2%-%TIME:~0,2%-%time:~3,2%-%time:~6,2%"
::
:: :: copy kDeployer files to project folder
:: xcopy /E/D/Y /EXCLUDE:..\common\common_excludedfileslist.txt "%SOURCE_DIR%\*" "%TARGET_DIR%"
::
:: :: copy project specific content
:: SET SOURCE_DIR="."
:: SET TARGET_DIR="%BASE_TARGET_DIR%\"
:: xcopy /E/D/Y /EXCLUDE:..\common\common_excludedfileslist.txt "%SOURCE_DIR%\*" "%TARGET_DIR%"
::
:: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
