# ------------------------------------------------------------------------------------
# filename: kDeployer.pl
# author:   Frode Klevstul (frode at klevstul dot com) (http://klevstul.com)
# started:  18.09.2013
# ------------------------------------------------------------------------------------

# --- requirements --------------------------------------------------------------------
# Regexp::Common::comment   :   https://metacpan.org/pod/distribution/Regexp-Common/lib/Regexp/Common/comment.pm
# -------------------------------------------------------------------------------------


# ------------------------------------------------------------------------------------
# use
# ------------------------------------------------------------------------------------
use strict;
use File::Copy;
use File::Compare;
use Cwd 'abs_path';                                                                                                                         # needed for being able to include kClosureCompiler from another dir ...
use File::Basename;                                                                                                                         # together with this ...
use lib dirname( abs_path $0 );                                                                                                             # and this.
use kClosureCompiler;
use Regexp::Common qw /comment/;


# ------------------------------------------------------------------------------------
# declarations
# ------------------------------------------------------------------------------------
my $debug                   = 0;                                                                                                            # debug mode: on (1) / off (0)

my $version                 = "v01_20170331";
my $help                    = "\nkDeployer [parameters] [file]\n"
                            . "--- Parameters: ---\n"
                            . "-f [file]          : specify file to deploy\n"
                            . "-full              : use this setting if providing full path names in -f. otherwise relative path to the scr dir is to be used\n"
                            . "-e [environment]   : what environment to deploy to. has to be defined in .ini file\n"
                            . "-l                 : list initialisation settings\n"
                            . "-min [filetype(s)] : minify file with given file type, removing new lines, double spacings etc\n"
                            . "                   : '*' for all types. if a list of types do separate with ','. example: 'js,json' (without any spaces)\n"
                            . "-rc [filetype(s)]  : raw copy of all files with these filetypes. these files will be copied directly, and not processed by kDeployer\n"
                            . "                   : '*' for all types. if a list of types do separate with ','. example: 'js,json' (without any spaces)\n"
                            . "-s                 : silent mode\n"
                            . "-ver               : show version number\n"
                            . "-help              : display this help\n"
                            . "--- - - - - - - ---\n"
                            . "Example:\n"
                            . "kDeployer.pl -e prod -f foo.bar\n";
my $heading                 = "kDeployer> ";                                                                                                # start of lines when printing

my $path                    = &getPath;                                                                                                     # retrieve the path to where this program is running
my $iniFile                 = $path . "kDeployer.ini";                                                                                      # the file containing all configuration values

my $silent                  = 0;                                                                                                            # runs in silent mode (1), without asking any questions or outputing any information
my $environment             = 0;                                                                                                            # environment to deploy to
my $src_file                = 0;                                                                                                            # the source file to deploy
my $minify                  = 0;                                                                                                            # minify resource to be deployed if set to 1
my $min_filelist            = 0;                                                                                                            # type of files to minify, '*' for all types
my $full                    = 0;                                                                                                            # full mode: true (1) / false (0) - if true then provide full path name to the file to be deployed. otherwise relative path name related the source directory
my $rawcopy                 = 0;                                                                                                            # rc / raw copy mode - if 1 then files will be copied and not processed
my $rc_filelist             = 0;                                                                                                            # types of files to do raw copy of, '*' for all types


# ------------------------------------------------------------------------------------
# main program
# ------------------------------------------------------------------------------------
getArguments();                                                                                                                             # get all arguments from command line
deploy();                                                                                                                                   # deploy file


# ------------------------------------------------------------------------------------
# sub routines
# ------------------------------------------------------------------------------------


# - - - - - - - - - - - - - - - - - - - - - - - - - -
# find the path to where the program is running
# - - - - - - - - - - - - - - - - - - - - - - - - - -
sub getPath{
    $0=~/^(.+[\\\/])[^\\\/]+[\\\/]*$/;
    my $cgidir = $1 || "./";
    return $cgidir;
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# load initial config values
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub getIniValue{
    my $parameter   = $_[0];
    my $list        = $_[1];
    my $line;
    my $value;

    $parameter =~ tr/a-z/A-Z/;                                                                                                              # convert the parameter to uppercase

    open(FILE, "<$iniFile") || kError("Failed to open '$iniFile'.");
    WHILE: while ($line = <FILE>){
        if ($line =~ m/^#/){
            next WHILE;
        }

        if ($list){
            if ($line =~ m/^(\w+)(\s)+(.*)$/){
                print $1 . " : " . $3 . "\n";
            }
        } else {
            if ($line =~ m/^$parameter(\s)+(.*)$/){
                $value = $2;
                last WHILE;
            }
        }
    }
    close (FILE);

    if ($value eq "" && !$list){
        kError("Failed to find values for parameter '$parameter' in '$iniFile'.",1);
    }

    kDebug("'$parameter' values read from .ini file.");

    return $value;
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# get arguments from user
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub getArguments{
    my $arg;
    my $e_read      = 0;                                                                                                                    # if we have read the parameter '-e' this becomes "true", then we know that this parameter's value will come next
    my $f_read      = 0;                                                                                                                    # if parameter '-f' is read
    my $min_read    = 0;                                                                                                                    # if parameter '-min' is read
    my $rc_read     = 0;                                                                                                                    # if parameter '-rc' is read

    foreach $arg (@ARGV){
        if ($arg =~ m/-help/ || $arg =~ m/-h/){                                                                                             # print out help and exit program
            kPrint($help);
            exit;
        } elsif ($arg =~ m/-ver/){
            kPrint("$version");
            exit;
        } elsif ($arg =~ m/^-/ || $e_read || $f_read || $min_read || $rc_read ) {                                                           # if we've got a parameter, or waiting to read a parameter's value
            if ($arg eq "-e"){
                kDebug("Environment argument read.");
                $e_read = 1;                                                                                                                # mark that we have read the -e parameter
            } elsif ($e_read && $arg){                                                                                                      # the '-e' parameter was read during previous loop, and here comes the value...
                kDebug("Environment: '$arg'.");
                $environment = $arg;
                $e_read = 0;
            } elsif ($arg eq "-f"){
                kDebug("File argument read.");
                $f_read = 1;                                                                                                                # mark that we have read the -f parameter
            } elsif ($f_read && $arg){                                                                                                      # the '-f' parameter was read during previous loop, and here comes the value...
                kDebug("File: '$arg'");
                $src_file = $arg;
                $f_read = 0;
            } elsif ($arg eq "-s"){
                kDebug("Program runs in silent mode.");
                $silent = 1;
            } elsif ($arg eq "-l"){
                kDebug("List init values.");
                getIniValue(0,1);
                exit;
            } elsif ($arg eq "-min"){
                kDebug("Minify argument read.");
                $min_read = 1;
            } elsif ($min_read && $arg){                                                                                                    # the '-min' parameter was read during previous loop, and here comes the value...
                kDebug("Minify mode enabled for files of type '$arg'.");
                $min_filelist = $arg;
                $minify = 1;
                $min_read = 0;
            } elsif ($arg eq "-full"){
                kDebug("Full path name mode.");
                $full = 1;
            } elsif ($arg eq "-rc"){
                kDebug("Rawcopy argument read.");
                $rc_read = 1;
            } elsif ($rc_read && $arg){                                                                                                     # the '-rc' parameter was read during previous loop, and here comes the value...
                kDebug("Rowcopy mode enabled for files of type '$arg'.");
                $rc_filelist = $arg;
                $rawcopy = 1;
                $rc_read = 0;
            } else {                                                                                                                        # unrecorgnised argument
                kError("Unrecorgnised argument '$arg' skipped.");
            }
        } else {                                                                                                                            # the filename comes here, if it was not given after an -f parameter
                $src_file = $arg;
                kDebug("File: '$src_file'.");
        }
    }

    if (!$environment){                                                                                                                     # check if user has specified a root directory where this program will "run" or check files
        kPrint($help);
        kError("Please specify the environment you want to deploy to.",1);
    } elsif (!$src_file) {
        kPrint($help);
        kError("Please specify the file you want to deploy.",1);
    }
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# print message to screen
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kPrint{
    my $text = $_[0];

    $text =~ s/\n/\n$heading/sgi;

    if (!$silent){
        print $heading . $text . "\n";
    }
}



# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# print debug message
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kDebug{
    my $text = $_[0];
    my $silent_turned_off = 0;

    if ($debug){
        if ($silent){                                                                                                                       # turns off silent mode to print debug information
            $silent = 0;
            $silent_turned_off = 1;
        }
        &kPrint("DEBUG: $text");
        $silent = 0;

        if ($silent_turned_off){                                                                                                            # if we have turned the silent mode off, turn it back on
            $silent = 1;
            $silent_turned_off = 0;
        }
    }
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# prints error message
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kError{
    my $text = $_[0];
    my $exit = $_[1];

    kPrint("ERROR: $text");

    if ($exit){
        exit;
    }
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# returns formatted timestamp
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kFormattedtimestamp{
	my $time = $_[0];
    my $type = $_[1];
	if ($time eq ""){
		$time = time;
	}
    if ($type eq ""){
        $type = "full";
    }

	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($time);
	my $timestamp;

	$year = $year+1900;
	$mon = $mon+1;

	# puts an zero in front of one digit.
	$sec =~ s/^(\d){1}$/0$1/;
	$min =~ s/^(\d){1}$/0$1/;
	$hour =~ s/^(\d){1}$/0$1/;
	$mday =~ s/^(\d){1}$/0$1/;
	$mon =~ s/^(\d){1}$/0$1/;

    # 160602181500
    if ($type eq "full") {
        $timestamp = substr($year,-2) . $mon . $mday . $hour . $min . $sec;
    } elsif ($type eq "split") {
        $timestamp = substr($year,-2) . $mon . $mday . "." . $hour . $min . $sec;
    } elsif ($type eq "short") {
        $timestamp = substr($year,-2) . $mon . $mday;
    }

	return $timestamp;
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# deploy file
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub deploy{
    my $env1_name           = getIniValue("ENV1_NAME");                                                                                     # name of environment 1
    my $env2_name           = getIniValue("ENV2_NAME");
    my $env3_name           = getIniValue("ENV3_NAME");
    my $env4_name           = getIniValue("ENV4_NAME");
    my $env5_name           = getIniValue("ENV5_NAME");
    my $file1_postfix       = getIniValue("FILE1_POSTFIX");                                                                                 # file type 1 ending / postfix
    my $file2_postfix       = getIniValue("FILE2_POSTFIX");
    my $file3_postfix       = getIniValue("FILE3_POSTFIX");
    my $file4_postfix       = getIniValue("FILE4_POSTFIX");
    my $file5_postfix       = getIniValue("FILE5_POSTFIX");
    my $file6_postfix       = getIniValue("FILE6_POSTFIX");
    my $file7_postfix       = getIniValue("FILE7_POSTFIX");
    my $file8_postfix       = getIniValue("FILE8_POSTFIX");
    my $file9_postfix       = getIniValue("FILE9_POSTFIX");
    my $file10_postfix      = getIniValue("FILE10_POSTFIX");
    my $file11_postfix      = getIniValue("FILE11_POSTFIX");
    my $file12_postfix      = getIniValue("FILE12_POSTFIX");
    my $file13_postfix      = getIniValue("FILE13_POSTFIX");
    my $file14_postfix      = getIniValue("FILE14_POSTFIX");
    my $file15_postfix      = getIniValue("FILE15_POSTFIX");
    my $buildversion_name   = getIniValue("BUILDVERSION_NAME");                                                                             # value found here is used to replace 'KDBUILD' string in filnames, if exists
    my $buildversion_value  = getIniValue("BUILDVERSION_VALUE");                                                                            # value found here is used to replace 'KDBUILD' string in filnames, if exists
    my $env_base            = getIniValue("ENV_BASE");                                                                                      # environment base tag
    my $src_dir             = getIniValue("SRC_DIR");                                                                                       # source directory for where the files to be deployd are stored
    my $tmp_dir             = getIniValue("TMP_DIR");                                                                                       # temporary directory used for file parsing, before copied to target (environment) directory
    my $interpreter         = getIniValue("INTERPRETER");                                                                                   # the tag for an interpreter line
    my $timestamp           = getIniValue("TIMESTAMP");                                                                                     # the tag for a timestamp
    my $datestamp_full      = getIniValue("DATESTAMP_FULL");                                                                                # the tag for datestamp, full version
    my $datestamp_split     = getIniValue("DATESTAMP_SPLIT");                                                                               # the tag for datestamp, split version
    my $datestamp_short     = getIniValue("DATESTAMP_SHORT");                                                                               # the tag for datestamp, short version
    my $noleadingspaces     = getIniValue("NOLEADINGSPACES");                                                                               # the tag for "no leading spaces"
    my $closure_compiler    = getIniValue("CLOSURE_COMPILER");                                                                              # list of files to use with google's closure compiler during minification
    my $regexp_cc           = getIniValue("REGEXP_CC");                                                                                     # list of files where "regexp common comment" is used for comment removal
    my $ignore_minification = getIniValue("IGNORE_MINIFICATION");                                                                           # list of files to ignore during the minification process

    my $comment;                                                                                                                            # the selected comment tag
    my $env_name;                                                                                                                           # name of selected environment
    my $env_tag;                                                                                                                            # tag of selected environment
    my $env_dir;                                                                                                                            # directory of selected environment
    my $interpreter_line;                                                                                                                   # interpreter line

    my $src_file_full;                                                                                                                      # path + filename for source file
    my $tmp_file_full;                                                                                                                      # path + filename for temporary file
    my $env_file_full;                                                                                                                      # path + filename for target file

    my $file_content;                                                                                                                       # the entire content of the file
    my $line;                                                                                                                               # one line of one file
    my $time = time();                                                                                                                      # seconds since epoch

    my $offset;                                                                                                                             # help variable, used for doing substring
    my $file_type;                                                                                                                          # help variable, used for storing file ending / file type
    my @min_filelist;                                                                                                                       # array of filetypes to minimise
    my @gcc_filelist;                                                                                                                       # array of files to use with closure compiler
    my @rcc_filelist;                                                                                                                       # array of files to use internal comment removal on
    my @nomin_filelist;                                                                                                                     # array of files to ignore during minification (no minification).
    my @rc_filelist;                                                                                                                        # array of filetype to "rawcopy"
    my $file_ending_in_rc_list;                                                                                                             # set to true, if file ending found in raw copy file type list
    my $file_in_gcc_list;                                                                                                                   # set to true, if file name is found in google's closure compiler list
    my $file_in_rcc_list;                                                                                                                   # set to true, if file name is found in internal comment removal list
    my $file_in_ignore_list;                                                                                                                # set to true, if file name is found in minification ignore list
    my $this_timestamp;                                                                                                                     # help value, for storing datestamp
    my $output_status;                                                                                                                      # string to be outputted, informing about status of the deploy.

    if ($src_file =~ m/\.$file1_postfix$/i){
        kDebug("file1_postfix.");
       $comment = getIniValue("COMMENT1");                                                                                                  # the comment tag for file type 1
    } elsif ($src_file =~ m/\.$file2_postfix$/i){
        kDebug("file2_postfix.");
       $comment = getIniValue("COMMENT2");
    } elsif ($src_file =~ m/\.$file3_postfix$/i){
        kDebug("file3_postfix.");
       $comment = getIniValue("COMMENT3");
    } elsif ($src_file =~ m/\.$file4_postfix$/i){
        kDebug("file4_postfix.");
       $comment = getIniValue("COMMENT4");
    } elsif ($src_file =~ m/\.$file5_postfix$/i){
        kDebug("file5_postfix.");
       $comment = getIniValue("COMMENT5");
    } elsif ($src_file =~ m/\.$file6_postfix$/i){
        kDebug("file6_postfix.");
       $comment = getIniValue("COMMENT6");
    } elsif ($src_file =~ m/\.$file7_postfix$/i){
        kDebug("file7_postfix.");
       $comment = getIniValue("COMMENT7");
    } elsif ($src_file =~ m/\.$file8_postfix$/i){
        kDebug("file8_postfix.");
       $comment = getIniValue("COMMENT8");
    } elsif ($src_file =~ m/\.$file9_postfix$/i){
        kDebug("file9_postfix.");
       $comment = getIniValue("COMMENT9");
    } elsif ($src_file =~ m/\.$file10_postfix$/i){
        kDebug("file10_postfix.");
       $comment = getIniValue("COMMENT10");
    } elsif ($src_file =~ m/\.$file11_postfix$/i){
        kDebug("file11_postfix.");
       $comment = getIniValue("COMMENT11");
    } elsif ($src_file =~ m/\.$file12_postfix$/i){
        kDebug("file12_postfix.");
       $comment = getIniValue("COMMENT12");
    } elsif ($src_file =~ m/\.$file13_postfix$/i){
        kDebug("file13_postfix.");
       $comment = getIniValue("COMMENT13");
    } elsif ($src_file =~ m/\.$file14_postfix$/i){
        kDebug("file14_postfix.");
       $comment = getIniValue("COMMENT14");
    } elsif ($src_file =~ m/\.$file15_postfix$/i){
        kDebug("file15_postfix.");
       $comment = getIniValue("COMMENT15");
    } else {
        if ($rawcopy) {
            if ($src_file =~ m!\.(\w+)$!i) {                                                                                                # get file type for file that is deployed
                $file_type = $1;
            }

            @rc_filelist = split(/,/, $rc_filelist);                                                                                        # get file types for file that will be raw copied

            foreach $_ (@rc_filelist) {                                                                                                     # loop through all rc file types given
                if ($_ =~ m!^$file_type$!i){                                                                                                # check if current file type equals a file type found in the rc file type list
                    kDebug("'$src_file' with file type '$file_type' is found in -rc argument '$rc_filelist', hence we will not process current file.");
                    $file_ending_in_rc_list = 1;
                }
            }
        }

        if (!$file_ending_in_rc_list) {                                                                                                     # if file type is not in rc (raw copy) list, and not defined in .ini file, we can't do much...
            kError("File '$src_file' has a file ending that is not defined in the .ini file.", 1);
        }
    }

    if ($environment eq $env1_name){                                                                                                        # check what environment the user typed in
        $env_name   = $env1_name;
        $env_tag    = getIniValue("ENV1_TAG");
        $env_dir    = getIniValue("ENV1_DIR");
    } elsif ($environment eq $env2_name){                                                                                                   # ... OK, not the most elegant piece of code, but it is readable
        $env_name   = $env2_name;
        $env_tag    = getIniValue("ENV2_TAG");
        $env_dir    = getIniValue("ENV2_DIR");
    } elsif ($environment eq $env3_name){
        $env_name   = $env3_name;
        $env_tag    = getIniValue("ENV3_TAG");
        $env_dir    = getIniValue("ENV3_DIR");
    } elsif ($environment eq $env4_name){
        $env_name   = $env4_name;
        $env_tag    = getIniValue("ENV4_TAG");
        $env_dir    = getIniValue("ENV4_DIR");
    } elsif ($environment eq $env5_name){
        $env_name   = $env5_name;
        $env_tag    = getIniValue("ENV5_TAG");
        $env_dir    = getIniValue("ENV5_DIR");
    } else {
        kError("Environment '$environment' was not found in initialisation file.", 1);
    }

    if ($full) {                                                                                                                            # if we are given the full path, we need to find the relative (to the source dir) path
        if (index($src_file, $src_dir) != -1) {                                                                                             # check if $src_dir is a substring of $src_file
            $offset = length($src_file) - length($src_dir);                                                                                 # calculate the offset, for the substring command
            $src_file = substr($src_file, length($src_dir) + 1, $offset);                                                                   # update $src_file, to only contain path name relative to the src folder
        } else {
            kError("'$src_file' is an invalid full path to a file in the kDeployer source directory. Try removing the '-full' attribute, or check the path to this file.", 1);
        }
    }

    $src_file_full = $src_dir . "/" . $src_file;
    $tmp_file_full = $tmp_dir . "/" . $src_file;
    $env_file_full = $env_dir . "/" . $src_file;
    $env_file_full =~ s/$buildversion_name/$buildversion_value/;                                                                            # replace possible build version in file names

    if ($rawcopy) {                                                                                                                         # if raw copy is enabled, we might move the file, without processing it
        @rc_filelist = split(/,/, $rc_filelist);

        foreach $_ (@rc_filelist) {                                                                                                         # loop through all file types we shall do a raw copy of
             if ($src_file =~ m/\.$_$/i){                                                                                                   # if we have a match...
                 if (compare($tmp_file_full, $env_file_full) != 0) {                                                                        # only copy file from tmp to target env dir if they differ
                    copy($src_file_full, $env_file_full) or kError("Unable to copy file '$src_file_full'\n to directory '$env_dir'.", 1);
                    kPrint("'$src_file' copied to '$env_name' environment, without being processed.");
                 } else {
                     kPrint("'$src_file' already exists in '$env_dir'.");
                 }
                exit;                                                                                                                       # nothing more to do, except for exiting
             }
        }
    }

    copy($src_file_full, $tmp_file_full) or kError("Unable to copy file '$src_file_full'\n to directory '$tmp_dir'.", 1);                   # copy file from source dir to temporary directory

    open (FILE, "<$tmp_file_full") || kError("Failed to open '$tmp_file_full'.");                                                           # open file and read the content
    flock (FILE, 2);                                                                                                                        # lock file for processing
    LINE: while ($line = <FILE>){
        if ($line =~ m/$env_base/){                                                                                                         # if the line matches the environment base tag / the environment prefix
            if ($line =~ m/$env_tag/){                                                                                                      # if line matches the full environment tag
                if ($line =~ m/$interpreter/){                                                                                              # if we have an interpreter line
                    $interpreter_line = $line;                                                                                              # we'll leave line alone, which means that all interpreter lines should start with comments (otherwise they will never be commented out)
                    $interpreter_line =~ s/$comment{1,}//g;                                                                                 # remove all comments
                    $interpreter_line =~ s/$env_base\w{1,}//g;                                                                              # remove all environment tags
                    $interpreter_line =~ s/$interpreter//g;                                                                                 # remove interpreter tag
                    $interpreter_line =~ s/^\s{1,}//g;                                                                                      # remove leading spaces
                    $interpreter_line =~ s/\s{1,}$//g;                                                                                      # remove trailing spaces
                } elsif ($line =~ m/^$comment/){                                                                                            # if line starts with a comment
                    $line =~ s/^($comment{1,}\s?)?//;                                                                                       # remove the comment (or multiple comments in a row) plus one space, if there is one
                    if ($line !~ m/$noleadingspaces/){                                                                                      # if the line does not contain a "no leading spaces" tag then pad it with spaces:
                        $line = " " x length($1) . $line;                                                                                   # ... length($1) equals the length of characters we removed above, we create a padded string, of spaces, that equals this length. avoids change of indentations and python failing.
                    }
                }
            } else {                                                                                                                        # ... line matches another environment than what the user typed in as an argument
                if ($line !~ m/^$comment/){                                                                                                 # if line doesn't start with a comment
                    $line = $comment . " " . $line;                                                                                         # add comment, and a space, to start of line
                }
            }
        }

        if ($line =~ m/$timestamp/){                                                                                                        # if we have a timestamp tag
            $line =~ s/$timestamp/$time/g;                                                                                                  # replace the tag with the number seconds-since-1970
        }

        if ($line =~ m/$datestamp_full/){                                                                                                   # if we have a datestamp tag (docu: http://www.tutorialspoint.com/perl/perl_date_time.htm)
            $this_timestamp = kFormattedtimestamp($time, "full");
            $line =~ s/$datestamp_full/$this_timestamp/g;                                                                                   # replace the tag with a timestamp on the format YYMMDDHHMISS
        }

        if ($line =~ m/$datestamp_split/){                                                                                                  # if we have a datestamp tag (docu: http://www.tutorialspoint.com/perl/perl_date_time.htm)
            $this_timestamp = kFormattedtimestamp($time, "split");
            $line =~ s/$datestamp_split/$this_timestamp/g;                                                                                  # replace the tag with a timestamp on the format YYMMDD.HHMISS
        }

        if ($line =~ m/$datestamp_short/){                                                                                                  # if we have a datestamp tag (docu: http://www.tutorialspoint.com/perl/perl_date_time.htm)
            $this_timestamp = kFormattedtimestamp($time, "short");
            $line =~ s/$datestamp_short/$this_timestamp/g;                                                                                  # replace the tag with a timestamp on the format YYMMDD
        }

        if ($line =~ m/$buildversion_name/){                                                                                                # if we have a build number tag
            $line =~ s/$buildversion_name/$buildversion_value/g;                                                                            # replace the tag with the build version
        }

        $file_content .= $line;                                                                                                             # build the parsed file content
    }
    flock (FILE, 8);                                                                                                                        # unlock file
    close (FILE);

    if ($minify) {
        @min_filelist = split(/\s?,\s?/, $min_filelist);
        @gcc_filelist = split(/\s?,\s?/, $closure_compiler);
        @rcc_filelist = split(/\s?,\s?/, $regexp_cc);
        @nomin_filelist = split(/\s?,\s?/, $ignore_minification);

        foreach $_ (@nomin_filelist) {                                                                                                      # loop through the ignore list
            if (index($src_file, $_) != -1){                                                                                                # check is file name is in the ignore list
                $output_status .= "file name found in ignore list. ";
                $file_in_ignore_list = 1;
            }
        }

        foreach $_ (@gcc_filelist) {                                                                                                        # loop through the gcc list
            if (index($src_file, $_) != -1 || $_ eq "\*") {                                                                                 # check if file name is in gcc list, or gcc list contains wildcard character '*'
                $output_status .= "file name found in closure compiler list. ";
                $file_in_gcc_list = 1;
            }
        }

        foreach $_ (@rcc_filelist) {                                                                                                        # loop through the regexp common comment list
            if (index($src_file, $_) != -1 || $_ eq "\*") {                                                                                 # check for file name or the wildcard character '*'
                $file_in_rcc_list = 1;
            }
        }

        foreach $_ (@min_filelist) {                                                                                                        # loop through all file types, to check if we shall minify the resource
             if ($src_file =~ m/\.$_$/i && $src_file !~ m/\.min\.$_$/i && !$file_in_ignore_list){                                           # if file type is specified to be minified + if the filename already do contain ".min." it most likely means it has already been minified, so we skip it.
                if ($src_file =~ m!\.(js|bar|css)$!i){                                                                                      # we support minification of .js, .css and .bar (latter only for testing purposes)
                    if ($file_in_gcc_list) {                                                                                                # check if we shall use the closure compiler for current file
                        my @result = kCC_compile($file_content);
                        if ($result[0] eq "SUCCESS"){
                            $output_status .= "minified by Closure Compiler. ";
                            $file_content = $result[1];
                        } else {
                            kError("Closure Compiler fail: $result[1]");
                        }
                        last;
                    } else {                                                                                                                # ... or we shall minify using internal minification code
                        if ($file_in_rcc_list) {
                            $output_status .= "minified by kDeployer w/ Regexp::Common::comment. ";
                            # removal of comments using Regexp::Common::comment
                            $file_content =~ s/($RE{comment}{ECMAScript})//g;                                                               # https://metacpan.org/pod/distribution/Regexp-Common/lib/Regexp/Common/comment.pm#ECMAScript
                            $file_content =~ s/($RE{comment}{HTML})//g;                                                                     # https://metacpan.org/pod/distribution/Regexp-Common/lib/Regexp/Common/comment.pm#HTML
                            $file_content =~ s/($RE{comment}{JavaScript})//g;                                                               # https://metacpan.org/pod/distribution/Regexp-Common/lib/Regexp/Common/comment.pm#JavaScript
                        } else {
                            $output_status .= "minified by kDeployer. ";
                            # internal comment removal
                            $file_content =~ s!/\*[^*]*\*+([^/*][^*]*\*+)*/|//([^\\]|[^\n][\n]?)*?\n|("(\\.|[^"\\])*"|'(\\.|[^'\\])*'|.[^/"'\\]*)!defined $3 ? $3 : ""!gse;     # http://perldoc.perl.org/perlfaq6.html#How-do-I-use-a-regular-expression-to-strip-C-style-comments-from-a-file%3f
                        }
                        $file_content =~ s/\R/ /g;                                                                                          # ref: http://stackoverflow.com/questions/881779/neatest-way-to-remove-linebreaks-in-perl
                        $file_content =~ s/\s/ /g;                                                                                          # make sure we are only using normal space, not tabs ( "\s" matches space, tab, newline )
                        $file_content =~ s/\s+/ /g;                                                                                         # remove two or more spaces after each other
                        $file_content .= "\n";                                                                                              # add trailing newline to the minified content
                        last;
                    }
                    kDebug("'$src_file' was minified.");
                }
            }
        }
    }

    open (FILE, ">$tmp_file_full") || kError("Failed to open '$tmp_file_full'.");                                                           # open the tmp file for writing
    flock (FILE, 2);                                                                                                                        # lock file
    if ($interpreter_line) {
        print FILE $interpreter_line . "\n";                                                                                                # write the interpreter line at the top of the file
    }
    print FILE $file_content;                                                                                                               # overwrite file with the parsed content
    flock (FILE, 8);                                                                                                                        # unlock file
    close (FILE);

    if (compare($tmp_file_full, $env_file_full) != 0) {                                                                                     # only copy file from tmp to target env dir if they are not equal
        copy($tmp_file_full, $env_file_full) or kError("Unable to copy file '$tmp_file_full'\n to directory '$env_dir'.", 1);               # copy file from temp dir to target directory
        kPrint("'$src_file' deployed to '$env_name' environment. $output_status");
    } else {
        kPrint("'$src_file' was already deployed and up-to-date in '$env_dir'.");
    }

}
