kDeployer
=========

A command line file deployment tool, written in Perl.

Example command:
> perl kDeployer.pl -e dev -f foo.bar

To understand what kDeployer can do, it's best to look at the examples below. Open the files to see the changes kDeployer does to a file.

**foo.bar**
- [original file](core/environments/_src/foo.bar)
- `perl kDeployer.pl -e dev -f foo.bar`
- [result](core/environments/dev/foo.bar)
- `perl kDeployer.pl -e prod -f foo.bar`
- [result](core/environments/prod/foo.bar)
 
**comment.bar**
- [original file](core/environments/_src/comment.bar)
- `perl kDeployer.pl -min bar -e prod -f comment.bar`
- [result](core/environments/prod/comment.bar)


# requirements

- [perl v5](https://www.perl.org/)
- [LWP::Protocol::https](https://metacpan.org/release/LWP-Protocol-https)
- [Regexp::Common](https://metacpan.org/release/Regexp-Common)
